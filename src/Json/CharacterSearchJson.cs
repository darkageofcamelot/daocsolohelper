using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace src.Json
{
    public class CharacterSearchResult {
        public string character_web_id  { get; set; }
        public string name { get; set; }

        public string server_name { get; set; }
        [JsonPropertyName("class_name")]
        public string ClassName { get; set; }

        [JsonPropertyName("realm_points")]
        public int RealmPoints { get; set; }
    }

    public class CharacterSearchJson {
        
        public List<CharacterSearchResult> results { get; set; }

    }
}