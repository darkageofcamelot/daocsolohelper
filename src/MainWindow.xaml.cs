﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace src
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Timer       checkLogFileTimer;
        public MainWindow()
        {
            InitializeComponent();

            checkLogFileTimer = new Timer( CheckLogFile, null, 10, Timeout.Infinite );

        }

        void CheckLogFile( object state ) {
            checkLogFileTimer.Dispose();

            if( File.Exists( Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Electronic Arts\\Dark Age of Camelot\\helper.log") ) {
                List<string> logList = new List<string>();
                try {
                    logList = new List<string>( File.ReadAllLines( Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Electronic Arts\\Dark Age of Camelot\\helper.log" ) );
                    if( logList.Count > 0 ) {
                        foreach( string line in logList) {
                            try {
                                Match match = Regex.Match(line, @"\[.*\] You target \[(?<targetName>.*)\].*", RegexOptions.IgnoreCase );
                                if( match.Success ) {
                                    UpdateTarget( match.Groups["targetName"].Value );
                                }
                            }catch( Exception exception ) {
                                MessageBox.Show( exception.Message );
                            }
                        }
                    }

                    File.Delete( Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Electronic Arts\\Dark Age of Camelot\\helper.log");

                } catch( IOException ) {
                    checkLogFileTimer = new Timer( CheckLogFile, null, 1000, Timeout.Infinite );
                    return;
                }
                /*try {
                    System.Windows.Application.Current.Dispatcher.Invoke( DispatcherPriority.Normal,      
                        (ThreadStart)delegate { targetName.Content = chatLogContent; });
                } catch( Exception exception ) {
                  */ 

            }
            
            checkLogFileTimer = new Timer( CheckLogFile, null, 10, Timeout.Infinite );
        }

        public void UpdateTarget( string characterName ) {
            string url = "http://api.camelotherald.com/character/search?name=/" + characterName + "&cluster=Ywain";

            HttpClient http = new HttpClient();

            try {
                HttpContent responseMessage = http.GetAsync( url ).Result.Content;

                if( responseMessage != null ) {
                    string characterData = responseMessage.ReadAsStringAsync().Result;
                        Json.ErrorJson errorJson = new Json.ErrorJson();
                        var options = new JsonSerializerOptions
                        {
                            AllowTrailingCommas = true
                        };

                        try {
                            errorJson = JsonSerializer.Deserialize<Json.ErrorJson>(characterData, options );
                        } catch( Exception exception ) {
                            MessageBox.Show( exception.ToString() );
                        }

                        if( errorJson.error != "" ) {
                            MessageBox.Show( "Error : " + errorJson.error );
                        } else {
                            try {
                                Json.CharacterSearchJson characterSearchJson = new Json.CharacterSearchJson();
                                characterSearchJson = JsonSerializer.Deserialize<Json.CharacterSearchJson>(characterData, options );

                                foreach( Json.CharacterSearchResult result in characterSearchJson.results ) {
                                    int indexSpace =  result.name.IndexOf( " ", 0, result.name.Length );
                                    if( indexSpace == characterName.Length || result.name.Length == characterName.Length ) {
                                        Character character = new Character( result.character_web_id, result.name, result.server_name, result.ClassName );
                                        character.RealmPoints = result.RealmPoints;
                                        System.Windows.Application.Current.Dispatcher.Invoke( DispatcherPriority.Normal,      
                                            (ThreadStart)delegate { targetName.Content = character.Name; className.Content = character.ClassName;realmRank.Content = character.RealmRank();nextRealmRank.Content = character.NextRealmRank();});

                                    }
                                }
                                

                                /*
                                character.RealmPoints = characterSearchJson.realm_war_stats.current.realm_points;
                                character.ClassName = characterSearchJson.ClassName;

                                //CharacterManager.CreateCharacter( character );
                                System.Windows.Application.Current.Dispatcher.Invoke( DispatcherPriority.Normal,      
                                        (ThreadStart)delegate { targetName.Content = character.Name; });
                                */
                            } catch( Exception exception ) {
                                MessageBox.Show( exception.ToString() );
                            }
                        }
  
                }     
            } catch( Exception exception) {

            } 
        }
    }
}
