namespace src {

    public  class Character {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ServerName { get; set; }
        public string ClassName { get; set; }
        public int RealmPoints  { get; set; }

        public Character() {
        }

        public Character( string id, string name, string serverName, string className ) {
            Id = id;
            Name = name;
            ServerName = serverName;
            ClassName = className;
            RealmPoints = 0;
        }

        public string RealmRank() {
            string realmRank = "1L0";

            if( RealmPoints < 1 ) {
                realmRank = "1l0";
            } else if( RealmPoints < 25 ) {
                realmRank = "1l1";
            } else if( RealmPoints < 125 ) {
                realmRank = "1l2";
            } else if( RealmPoints < 350 ) {
                realmRank = "1l3";
            } else if( RealmPoints < 750 ) {
                realmRank = "1l4";
            } else if( RealmPoints < 1375 ) {
                realmRank = "1l5";
            } else if( RealmPoints < 2275 ) {
                realmRank = "1l6";
            } else if( RealmPoints < 3500 ) {
                realmRank = "1l7";
            } else if( RealmPoints < 5100 ) {
                realmRank = "1l8";
            } else if( RealmPoints < 7125 ) {
                realmRank = "1l9";
            } else if( RealmPoints < 9625 ) {
                realmRank = "2l0";
            } else if( RealmPoints < 12650 ) {
                realmRank = "2l1";
            } else if( RealmPoints < 61750 ) {
                realmRank = "2lx";
            } else if( RealmPoints < 71750 ) {
                realmRank = "3l0";
            } else if( RealmPoints < 82775 ) {
                realmRank = "3l1";
            } else if( RealmPoints < 94875 ) {
                realmRank = "3l2";
            } else if( RealmPoints < 108100 ) {
                realmRank = "3l3";
            } else if( RealmPoints < 122500 ) {
                realmRank = "3l4";
            } else if( RealmPoints < 138125 ) {
                realmRank = "3l5";
            } else if( RealmPoints < 155025 ) {
                realmRank = "3l6";
            } else if( RealmPoints < 173250 ) {
                realmRank = "3l7";
            } else if( RealmPoints < 192850 ) {
                realmRank = "3l8";
            } else if( RealmPoints < 213875 ) {
                realmRank = "3l9";
            } else if( RealmPoints < 236375 ) {
                realmRank = "4l0";
            } else if( RealmPoints < 260400 ) {
                realmRank = "4l1";
            } else if( RealmPoints < 513500 ) {
                realmRank = "4lx";
            } else if( RealmPoints < 1010625 ) {
                realmRank = "5lx";
            } else if( RealmPoints < 1755250 ) {
                realmRank = "6lx";
            } else if( RealmPoints < 1845250 ) {
                realmRank = "7l0";
            } else if( RealmPoints < 1938275 ) {
                realmRank = "7l1";
            } else if( RealmPoints < 2797375 ) {
                realmRank = "7lx";
            } else if( RealmPoints < 4187000 ) {
                realmRank = "8lx";
            } else if( RealmPoints < 5974125 ) {
                realmRank = "9lx";
            } else if( RealmPoints < 6176625 ) {
                realmRank = "10l0";
            } else if( RealmPoints < 8208750 ) {
                realmRank = "10lx";
            } else if( RealmPoints < 9111713 ) {
                realmRank = "11l0";
            } else if( RealmPoints < 10114001 ) {
                realmRank = "11l1";
            } else if( RealmPoints < 11226541 ) {
                realmRank = "11l2";
            } else if( RealmPoints < 12461460 ) {
                realmRank = "11l3";
            } else if( RealmPoints < 23308097 ) {
                realmRank = "11lx";
            } else if( RealmPoints < 66181501 ) {
                realmRank = "12lx";
            } else if( RealmPoints < 187917143 ) {
                realmRank = "13lx";
            } else {
                realmRank = "14l0";
            }

            return realmRank;
        }

        public string NextRealmRank() {int nextRealmRank = 0;
            int realmPoints = RealmPoints;

            if( realmPoints < 1 ) {
                nextRealmRank = 1;
            } else if( realmPoints < 25 ) {
                nextRealmRank = 25;
            } else if( realmPoints < 125 ) {
                nextRealmRank = 125;
            } else if( realmPoints < 350 ) {
                nextRealmRank = 350;
            } else if( realmPoints < 750 ) {
                nextRealmRank = 750;
            } else if( realmPoints < 1375 ) {
                nextRealmRank = 1375;
            } else if( realmPoints < 2275 ) {
                nextRealmRank = 2275;
            } else if( realmPoints < 3500 ) {
                nextRealmRank = 3500;
            } else if( realmPoints < 5100 ) {
                nextRealmRank = 5100;
            } else if( realmPoints < 9625 ) {
                nextRealmRank = 9625;
            } else if( realmPoints < 12650 ) {
                nextRealmRank = 12650;
            } else if( realmPoints < 16250 ) {
                nextRealmRank = 16250;
            } else if( realmPoints < 20475 ) {
                nextRealmRank = 20475;
            } else if( realmPoints < 25375 ) {
                nextRealmRank = 25375;
            } else if( realmPoints < 108100 ) {
                nextRealmRank = 108100;
            } else if( realmPoints < 192850 ) {
                nextRealmRank = 192850;
            } else if( realmPoints < 10114001 ) {
                nextRealmRank = 10114001;
            } else if( realmPoints < 11226541 ) {
                nextRealmRank = 11226541;
            } 

            return ( nextRealmRank - realmPoints ).ToString();
        }
    }
}
