using System;
using System.Windows.Data;

namespace src
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class RealmPointsConverter : IValueConverter {
       public object Convert(object value, Type targetType, object parameter,
                      System.Globalization.CultureInfo culture)        
        {            
            string realmRank;
            int realmPoints = ( int )value;

            if( realmPoints >= 25 && realmPoints < 125 ) {
                realmRank = "1L2";
            } else if( realmPoints >= 125 && realmPoints < 350 ) {
                realmRank = "1L3";
            } else if( realmPoints >= 350 && realmPoints < 750 ) {
                realmRank = "1L4";
            } else if( realmPoints >= 750 && realmPoints < 1375 ) {
                realmRank = "1L5";
            } else if( realmPoints >= 1375 && realmPoints < 2275 ) {
                realmRank = "1L6";
            } else if( realmPoints >= 2275 && realmPoints < 3500 ) {
                realmRank = "1L7";
            } else if( realmPoints >= 2275 && realmPoints < 3500 ) {
                realmRank = "1L8";
            } else if( realmPoints >= 3500 && realmPoints < 5100 ) {
                realmRank = "1L9";
            } else if( realmPoints >= 7125 && realmPoints < 9625 ) {
                realmRank = "2L0";
            } else if( realmPoints >= 9625 && realmPoints < 12650 ) {
                realmRank = "2L1";
            } else if( realmPoints >= 12650 && realmPoints < 16250 ) {
                realmRank = "2L2";
            } else if( realmPoints >= 16250 && realmPoints < 20475 ) {
                realmRank = "2L3";
            } else if( realmPoints >= 20475 && realmPoints < 25375 ) {
                realmRank = "2L4";
            } else if( realmPoints >= 94875 && realmPoints < 108100 ) {
                realmRank = "3L3";
            } else if( realmPoints >= 173250 && realmPoints < 192850 ) {
                realmRank = "3L8";
            } else if( realmPoints >= 9111713 && realmPoints < 10114001 ) {
                realmRank = "11L1";
            } else if( realmPoints >= 10114001 && realmPoints < 11226541 ) {
                realmRank = "11L2";
            } else {
                realmRank = "1L1";
            }

            return realmRank.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter,
                    System.Globalization.CultureInfo culture) {
            return null; 
        }  
 
    }
}