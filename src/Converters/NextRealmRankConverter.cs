using System;
using System.Windows.Data;

namespace src
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class NextRealmRankConverter : IValueConverter {
                public object Convert(object value, Type targetType, object parameter,
                      System.Globalization.CultureInfo culture)        
        {            
            int nextRealmRank = 0;
            int realmPoints = ( int )value;

            if( realmPoints < 1 ) {
                nextRealmRank = 1;
            } else if( realmPoints < 25 ) {
                nextRealmRank = 25;
            } else if( realmPoints < 125 ) {
                nextRealmRank = 125;
            } else if( realmPoints < 350 ) {
                nextRealmRank = 350;
            } else if( realmPoints < 750 ) {
                nextRealmRank = 750;
            } else if( realmPoints < 1375 ) {
                nextRealmRank = 1375;
            } else if( realmPoints < 2275 ) {
                nextRealmRank = 2275;
            } else if( realmPoints < 3500 ) {
                nextRealmRank = 3500;
            } else if( realmPoints < 5100 ) {
                nextRealmRank = 5100;
            } else if( realmPoints < 9625 ) {
                nextRealmRank = 9625;
            } else if( realmPoints < 12650 ) {
                nextRealmRank = 12650;
            } else if( realmPoints < 16250 ) {
                nextRealmRank = 16250;
            } else if( realmPoints < 20475 ) {
                nextRealmRank = 20475;
            } else if( realmPoints < 25375 ) {
                nextRealmRank = 25375;
            } else if( realmPoints < 108100 ) {
                nextRealmRank = 108100;
            } else if( realmPoints < 192850 ) {
                nextRealmRank = 192850;
            } else if( realmPoints < 10114001 ) {
                nextRealmRank = 10114001;
            } else if( realmPoints < 11226541 ) {
                nextRealmRank = 11226541;
            } 

            return ( nextRealmRank - realmPoints );
        }
        
        public object ConvertBack(object value, Type targetType, object parameter,
                    System.Globalization.CultureInfo culture) {
            return null; 
        }  
 
    }
}